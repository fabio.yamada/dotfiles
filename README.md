# dotfiles

Repository to keep my personal Mac OS dotfiles.

## Description
This is my personal configuration files repository to allow reusability and history tracking of several productivity tools used in my zsh environment.

## List of tools covered
- zsh
    - [powerlevel10k](https://github.com/romkatv/powerlevel10k)
        - Nerd Fonts
    - [omh](https://ohmyz.sh/)
        - git
        - colored-man-pages
        - colorize
        - dircycle
        - web-search
    - tmux
- nvim: with lua configs
    - Packer
    - hrsh7th/nvim-cmp: autocomplete features
    - lewins6991/gitsigns.nvim
    - iamcco/markdown-preview.nvim
    - nvim-treesitter/nvim-treesitter
    - glacambre/firevim
    - nvim-lualine/lualine.nvim
    - tjdevries/colorbuddy.vim
    - dracula/vim
    - neovim/nvim-lspconfig
    - nvim-telescope/telescope.nvim
    - preservim/nerdtree

## Installation
GNU Stow is required to sync repo to home folder configuration files.

## Usage
This repo was structured in a way to integrate with GNU Stow on a Mac OS environment. It is very likely to work seamlessly on linux environments.
After cloning the repo, use the stow command to craete symlinks to the configuration files, noting that there should be no current file on the target destination.
```
stow -t ~/ -R <module-name>
```
On the repository root, each module is hosted in a first level folder (i.e. nvim). The idea behing the folder organization is to group related configurations together. Whatever is within the module folder, stow will attempt to create symlinks at the specified path having the home folder as starting point (because we use -t flag poiting to the home folder).
If using zsh, after the first load of this repo .zshrc file, an alias is delcared to automatically map stow to the `-t ~/ -R` options to allow using `stow <module-name>` directly.

## License
MIT

