# Setup fzf
# ---------
if [[ ! "$PATH" == */opt/homebrew/opt/fzf/bin* ]]; then
  PATH="${PATH:+${PATH}:}/opt/homebrew/opt/fzf/bin"
fi

# Auto-completion
# ---------------
FILE=/opt/homebrew/opt/fzf/shell/completion.zsh
if ! test -f "$FILE"; then
    FILE=/usr/share/doc/fzf/examples/completion.zsh
fi
[[ $- == *i* ]] && source "$FILE" 2> /dev/null

# Key bindings
FILE=/opt/homebrew/opt/fzf/shell/key-bindings.zsh
if ! test -f "$FILE"; then
    FILE=/usr/share/doc/fzf/examples/key-bindings.zsh
fi
[[ $- == *i* ]] && source "$FILE" 2> /dev/null
# ------------
