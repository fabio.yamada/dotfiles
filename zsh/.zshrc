# Enable Powerlevel10k instant prompt. Should stay close to the top of ~/.zshrc.
# Initialization code that may require console input (password prompts, [y/n]
# confirmations, etc.) must go above this block; everything else may go below.
if [[ -r "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh" ]]; then
  source "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh"
fi

# source all work related config
source ~/.workenvs

# font reference to download and use on terminal configuration that works with powerlevel10k
# # MESLO
# https://github.com/romkatv/powerlevel10k-media/raw/master/MesloLGS%20NF%20Regular.ttf 
# https://github.com/romkatv/powerlevel10k-media/raw/master/MesloLGS%20NF%20Bold.ttf
# https://github.com/romkatv/powerlevel10k-media/raw/master/MesloLGS%20NF%20Italic.ttf
# https://github.com/romkatv/powerlevel10k-media/raw/master/MesloLGS%20NF%20Bold%20Italic.ttf
# DejaVu (need to install all font formats (Regular, Italic, Bold, Italic Bold))
# https://github.com/ryanoasis/nerd-fonts/raw/master/patched-fonts/DejaVuSansMono/Regular/complete/DejaVu%20Sans%20Mono%20Nerd%20Font%20Complete.ttf
# https://github.com/ryanoasis/nerd-fonts/raw/master/patched-fonts/DejaVuSansMono/Italic/complete/DejaVu%20Sans%20Mono%20Oblique%20Nerd%20Font%20Complete.ttf
# https://github.com/ryanoasis/nerd-fonts/raw/master/patched-fonts/DejaVuSansMono/Bold/complete/DejaVu%20Sans%20Mono%20Bold%20Nerd%20Font%20Complete.ttf
# https://github.com/ryanoasis/nerd-fonts/raw/master/patched-fonts/DejaVuSansMono/Bold-Italic/complete/DejaVu%20Sans%20Mono%20Bold%20Oblique%20Nerd%20Font%20Complete.ttf

# Path to your oh-my-zsh installation.
export ZSH="$HOME/.oh-my-zsh"
export NODE_VERSION=16.15.0
export NVS_HOME="$HOME/.nvs"
export NVM_DIR="$HOME/.nvm"
export CXXFLAGS="-mmacosx-version-min=11.4"
export LDFLAGS="-mmacosx-version-min=11.4"
export PATH="$HOME/.local/bin:/usr/local/bin:$HOME/bin:$PATH"
export PATH="$PATH:$HOME/.local/share/nvim/lsp_servers/jdtls/bin"
export PATH="$PATH:$HOME/opt/lua-language-server/bin"
[ -s "$NVM_DIR/nvm.sh" ] && . "$NVM_DIR/nvm.sh"
[ -s "$NVS_HOME/nvs.sh" ] && . "$NVS_HOME/nvs.sh"
# Allow puppeteer on Apple Silicon
export PUPPETEER_SKIP_CHROMIUM_DOWNLOAD=true
export PUPPETEER_EXECUTABLE_PATH=`which chromium`

# Set name of the theme to load --- if set to "random", it will
# load a random theme each time oh-my-zsh is loaded, in which case,
# to know which specific one was loaded, run: echo $RANDOM_THEME
# See https://github.com/ohmyzsh/ohmyzsh/wiki/Themes
ZSH_THEME="powerlevel10k/powerlevel10k"

zstyle ':omz:update' mode reminder  # just remind me to update when it's time
# Which plugins would you like to load?
# Standard plugins can be found in $ZSH/plugins/
# Custom plugins may be added to $ZSH_CUSTOM/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.
plugins=(git colored-man-pages colorize dircycle web-search)

source $ZSH/oh-my-zsh.sh

# neofetch displays some OS info in a visual mode
#neofetch

# Enable vi mode for zsh commandline
bindkey -v
if command -v pyenv 1>/dev/null 2>&1; then
  eval "$(pyenv init -)"
fi
alias vi=nvim
alias vim=nvim
# make stow handle dotfiles git repository by default
alias stow='stow -t ~/ -R $1'

# make chromium be found by puppeteer
alias chromium-browser=/opt/homebrew/bin/chromium

# To customize prompt, run `p10k configure` or edit ~/.p10k.zsh.
[[ ! -f ~/.p10k.zsh ]] || source ~/.p10k.zsh
#############
#FZF fuzzy finder setup
# Feed the output of fd into fzf
#fd --type f --strip-cwd-prefix | fzf
# Setting fd as the default source for fzf
export FZF_DEFAULT_COMMAND='fd --type f --strip-cwd-prefix -HI'
# Now fzf (w/o pipe) will use fd instead of find
# To apply the command to CTRL-T as well
export FZF_CTRL_T_COMMAND="$FZF_DEFAULT_COMMAND"
[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh
############ FZF END

########
#TMUX
###
alias tmux=tmux -CC -f ~/.config/tmux/tmux.conf
######## TMUX END
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion
alias luamake=~/download/lua-language-server/3rd/luamake/luamake
