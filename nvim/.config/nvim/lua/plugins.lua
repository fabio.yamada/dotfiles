-- This file can be loaded by calling `lua require('plugins')` from your init.vim

-- Only required if you have packer configured as `opt`
vim.cmd [[packadd packer.nvim]]

return require('packer').startup(function(use)
	-- Packer can manage itself
	use 'wbthomason/packer.nvim'

	-- Simple plugins can be specified as strings
	--use '9mm/vim-closer'

	-- Lazy loading:
	-- Load on specific commands
	use { 'tpope/vim-dispatch', opt = true, cmd = { 'Dispatch', 'Make', 'Focus', 'Start' } }

	-- Load on an autocommand event
	use { 'andymass/vim-matchup', event = 'VimEnter' }

	-- Load on a combination of conditions: specific filetypes or commands
	-- Also run code after load (see the "config" key)
	use {
		'w0rp/ale',
		ft = { 'sh', 'zsh', 'bash', 'c', 'cpp', 'cmake', 'html', 'markdown', 'racket', 'vim', 'tex' },
		cmd = 'ALEEnable',
		config = 'vim.cmd[[ALEEnable]]'
	}

	-- Plugins can have dependencies on other plugins
	use {
		'hrsh7th/nvim-cmp',
		requires = {
			{ 'hrsh7th/cmp-buffer' },
			{ 'hrsh7th/cmp-nvim-lsp-signature-help' },
			{ 'hrsh7th/cmp-path' },
			{ 'hrsh7th/cmp-path' },
			{ 'hrsh7th/cmp-cmdline' },
			{ 'hrsh7th/cmp-nvim-lsp' },
			{ 'hrsh7th/cmp-vsnip' },
			{ 'hrsh7th/vim-vsnip' },
			{ 'onsails/lspkind.nvim' }
		}
	}
    use {
        'mfussenegger/nvim-dap-python',
        requires = {
            {'mfussenegger/nvim-dap'},
            {'microsoft/debugpy'},
            {'rcarriga/nvim-dap-ui'},
            {'folke/neodev.nvim'},
            {'theHamsta/nvim-dap-virtual-text'}
        }
    }

    use { "mxsdev/nvim-dap-vscode-js", requires = {"mfussenegger/nvim-dap"} }

    use {
      "microsoft/vscode-js-debug",
      opt = true,
      run = "npm install --legacy-peer-deps && npx gulp vsDebugServerBundle && mv dist out" 
    }

	-- Plugins can have post-install/update hooks
    -- use({
    --      "iamcco/markdown-preview.nvim",
    --    run = function() vim.fn["mkdp#util#install"]() end,
    --})

    use({ "iamcco/markdown-preview.nvim", run = "cd app && npm install", setup = function() vim.g.mkdp_filetypes = { "markdown" } end, ft = { "markdown" }, })


	-- Post-install/update hook with neovim command
	use { 'nvim-treesitter/nvim-treesitter', run = ':TSUpdate' }

	-- Post-install/update hook with call of vimscript function with argument
	use { 'glacambre/firenvim', run = function() vim.fn['firenvim#install'](0) end }

	use {
		'nvim-lualine/lualine.nvim',
		requires = { { 'sainnhe/gruvbox-material' }, { 'kyazdani42/nvim-web-devicons', opt = true } }
	}

	-- Use dependency and run lua function after load
	use {
		'lewis6991/gitsigns.nvim', requires = { 'nvim-lua/plenary.nvim' },
		config = function() require('gitsigns').setup() end
	}

	use { 'tjdevries/colorbuddy.vim', opt = true }

	-- Themes 
	use { 'dracula/vim', as = 'dracula' }
    use { 'folke/tokyonight.nvim' }
    use { 'catppuccin/nvim', as = 'catppuccin' }

	-- LSP
	use {
        'williamboman/nvim-lsp-installer',
        'neovim/nvim-lspconfig',
    }
    use {
        'mfussenegger/nvim-jdtls'
    }
    use {
      'nvim-telescope/telescope.nvim', tag = '0.1.0',
      requires = { {'nvim-lua/plenary.nvim'} },
      config = function() require('telescope').setup({
        pickers = {
            find_files = {
                hidden = true
            }
        },
        mappings = {
            n = {
                ['<C-d>'] = require('telescope.actions').delete_buffer
            }, -- n
            i = {
                ['<C-d>'] = require('telescope.actions').delete_buffer
            } -- i
        }
      }) end
    }

	-- NERD treesitter
	use {
			'preservim/nerdtree',
			requires = {'Xuyuanp/nerdtree-git-plugin', 'ryanoasis/vim-devicons'}
	}

    use {'github/copilot.vim'}
end)
