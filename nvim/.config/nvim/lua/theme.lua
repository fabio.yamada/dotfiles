-- Theme preparation
local custom_gruvbox = require('lualine.themes.jellybeans')

require('lualine').setup {
    options = {
        theme  = 'catppuccin',
        component_separators = { left = '', right = ' '},
        section_separators = { left = '  ', right = ''}
    },
    sections = {
        lualine_c = {'%f'}
    }
}

vim.cmd[[colorscheme catppuccin-macchiato]]
vim.api.nvim_set_hl(0, "Normal", { bg = "none" })
vim.api.nvim_set_hl(0, "NormalFloat", { bg = "none" })
