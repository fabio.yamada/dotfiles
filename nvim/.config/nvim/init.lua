-- Global vim.options
vim.opt.relativenumber = true
vim.opt.number = true
vim.opt.softtabstop = 4
vim.opt.shiftwidth = 4
vim.opt.tabstop = 4
vim.opt.encoding = 'UTF-8'
vim.cmd('set expandtab')
vim.cmd('set foldmethod=indent')
vim.cmd('set colorcolumn=150')
vim.cmd('set cursorline')
vim.cmd('set cursorcolumn')
vim.cmd('let NERDTreeShowHidden=1')
vim.cmd('set listchars=tab:→\\ ,space:·,nbsp:␣,trail:•,eol:¶,precedes:«,extends:»')
--vim.cmd('set list')
-- Packer Plugins
require('plugins')
-- Lualine status and theme
require('theme')
-- lsp
require('lsp-bootstrap')
-- dap configuration
require('dap-bootstrap');
-- telescope configuration
require('telescope').setup()
local builtin = require('telescope.builtin')
vim.keymap.set('n', 'ff', builtin.find_files, {})
vim.keymap.set('n', 'fg', builtin.live_grep, {})
vim.keymap.set('n', 'fb', builtin.buffers, {})
vim.keymap.set('n', 'fh', builtin.help_tags, {})
